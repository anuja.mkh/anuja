//Example 1

//type of variables   primitive datatypes

/* var a = 25;
 var b = "Hello";
 var c = true;
 var d = undefined;
 var e = null;
console.log(typeof a);  //number
console.log(typeof b)   //string
 console.log(typeof c);  //boolean
 console.log(typeof d)   //undefined
/console.log(typeof e)    // object   tech debt */


//Example 2

 var num = 20;
 console.log("1", num);  //20
 num = 30;
 function fuNumber() {
   
      console.log("2", num);  //40
}
num = 40;
 fuNumber();



//Example 3

// var num = 20;
// console.log("1", num);  //20
// num = 30;
// function fuNumber() {
//      num = 60;                      //only functional scope
//     console.log("3", num);           //60      
//     var num;                          //get hoisted on top of the function
// }   
// num = 40;
// console.log("2", num);  //40
// fuNumber();


//Example 4

// var num ;
// console.log("1", num);  //undefined
// num = 30;
// function fuNumber() {
//      num = 60;                      //only functional scope
//     console.log("3", num);           //60      
//     var num;                       //get hoisted on top of the function
//     function innerFun() {
//         console.log("5",num)      //60
//     }
//     console.log("4",num)   //60
//     innerFun();
// }
// console.log("2",num)   //30
// fuNumber();
// num = 40;
// console.log("6", num);  //40


//Example 5

// var num ;
// console.log("1", num);  //undefined
// num = 30;
// function fuNumber() {                    
//     console.log("3", num);   //undefined       
//     function innerFun() {
//         console.log("5",num)      //undefined  is undefined because num = 60 is assign after calling innerFn()
//     }
//     console.log("4",num)   //undefined
//     innerFun();
//     var num = 60;    //get hoisted   
// }
// console.log("2",num)   //30
// fuNumber();
// console.log("6", num);  //30


//Example 6


// var num ;
// console.log("1", num);  //undefined
// num = 30;
// function fuNumber() {                    
//     console.log("3", num);   //undefined       
//     function innerFun() {
//         console.log("5",num)      //60
//     }
//     console.log("4", num)   //undefined
//     var num = 60;    //get hoisted  
//     innerFun();
    
// }
// console.log("2",num)   //30
// fuNumber();
// console.log("6", num);  //30



//Example 7

//let and const 

// let num ;
// console.log("1", num);   //undefined
// num = 30;
// function fuNumber() {                    
//     console.log("3", num);        //error
//     function innerFun() {
//         console.log("5",num)      
//     }
//     console.log("4", num)   
//     let num = 60;               //let doesnt hoisted 
//     innerFun();
    
// }
// console.log("2",num)  //30
// fuNumber();
// console.log("6", num);




//Example 8

// let num ;
// console.log("1", num);   //undefined
// num = 30;
// function fuNumber() {
//     let num = 60; 
//     console.log("3", num);      //60
//     function innerFun() { 
//         console.log("5",num)     //60 
//     }
//     console.log("4", num)    //60
                
//     innerFun();
    
// }
// console.log("2",num)  //30
// fuNumber();
// console.log("6", num);  



//Example 9

// let num ;
// console.log("1", num);   //undefined
// num = 30;
// function fuNumber() {
//      num = 60; 
//     console.log("3", num);      //60
//     function innerFun() {
//         let num = 70; 
//         console.log("5",num)     //70 
//     }
//     console.log("4", num)    //60
                
//     innerFun();
//     console.log("6",num)    //60
// }
// console.log("2",num)  //30
// fuNumber();
// console.log("7", num); 



//Example 10

// let num ;
// console.log("1", num);   //undefined
// num = 30;
// function fuNumber() {
//      num = 60;      //error
//     console.log("3", num);      
//     function innerFun() {
//         let num = 70; 
//         console.log("5",num)     
//     }
//     console.log("4", num)           
//     innerFun();
//     let num = 90;
//     console.log("6",num)    
// }
// console.log("2",num)  //30
// fuNumber();
// console.log("7", num);


//Example 11

// let num ;
// console.log("1", num);   //undefined
// num = 30;
// function fuNumber() { 
//     console.log("3", num);      //30
//     function innerFun() {
//         let num = 70; 
//         console.log("4", num)    //70
//         function innerToInner() {
//             console.log("5", num)     //70 
//         }
//         innerToInner()
//     }
//     innerFun();
//     console.log("6",num)    //30
// }
// console.log("2",num)  //30
// fuNumber();
// console.log("7", num); //30



//Example 12

//initialise must if we used const

// const num =30;   //error         Missing initializer in const declaration
// console.log("1", num);   
// function fuNumber() {
//      num = 60;             //error doesn't reassign the const value
//     console.log("3", num);      
//     function innerFun() {
//         let num = 70; 
//         console.log("5",num)     
//     }
//     console.log("4", num)           
//     innerFun();
//     console.log("6",num)    
// }
// console.log("2",num)  //30
// fuNumber();
// console.log("7", num);








